<?php
    global $controllers;

    $controllers->add('example', function() {
        global $viewData;
        $viewData->setContent('example', 'Example !');
    });
