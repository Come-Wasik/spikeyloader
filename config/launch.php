<?php

// Inclusion and starting of the framework
require_once(__DIR__.'/../model/framework/SpikeyLoader.class.php');
$framework = new Spikeyloader();

$router = $framework->getRouter();
$controllers = $framework->getControllers();
$viewData = $framework->getViewData();
// Launch operations (before generation of the page)
//session_start();

// Page generation
$framework->start();