<?php

try {
    // Start the project
    require_once(__DIR__.'/config/launch.php');
} catch(Exception $e) {

    echo '<style>
        table#error, #error td, #error tr {
            border: solid 1px rgb(35, 216, 29);
            border-collapse: collapse;
            background-color: rgb(142, 78, 216);
            padding: 5px;
            color: white;
        }
        
        </style>';
    echo '<table id="error"><tr><td>Erreur n°'. $e->getCode().' - '.$e->getmessage().'</td></tr></table>';
}
?>