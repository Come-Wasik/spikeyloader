<?php
global $router;

// Front
$router->addPage('main', Router::SHOW_ONLY);

// Back
$router->addPage('example', Router::CONTROL_AND_SHOW);