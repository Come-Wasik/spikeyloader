<?php
class RouterPage {
    private $name;
    private $state;
    private $launchCondition;

    public function __construct($name, $state) {
        $this->setName($name);
        $this->setState($state);
    }

    public function getName() : string { return $this->name; }
    public function getState() : int { return $this->state; }

    public function setName(string $newData) : void { $this->name = $newData; }
    public function setState(string $newData) : void { $this->state = $newData; }
}