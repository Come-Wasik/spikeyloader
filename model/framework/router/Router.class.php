<?php
require_once(__DIR__.'/RouterPage.class.php');

class Router {
    /* Created by Nolikein


    */
    private $listPages = [];
    private $pageIdentificator = 'act';
    private $mainPageId = 'main';
    private $error404Id = 'error404';

    const SHOW_ONLY = 0;
    const CONTROL_AND_SHOW = 1;
    const CONTROL_ONLY = 2;

    public function __construct() {
        $this->addPage('error404', Router::SHOW_ONLY);
    }

    public function addPage(string $name, int $state) : bool {
        if(!preg_match('#\.\.[/\\\]#', $name)) {
            $this->listPages[] = new RouterPage($name, $state);
            return true;
        }
        else
            return false;
    }

    public function start() : void {
        $pageIdentificator = (!empty($getValue = $_GET[$this->pageIdentificator] ?? "") ? $getValue : $this->mainPageId);

        if($page = $this->getPage($pageIdentificator)) {
            if($page->getState() == Router::SHOW_ONLY)
                $this->showPage($page->getName());
            else if($page->getState() == Router::CONTROL_AND_SHOW) {
                $this->useController($page->getName());
                $this->showPage($page->getName());
            }
            else
                $this->useController($page->getName());
        }
        else {
            $this->showPage($this->error404Id);
        }
    }

    private function showPage(string $pageName) : void {
        if(!preg_match('#\.\.[/\\\]#', $pageName))
            include_once(__DIR__.'/../../../view/'.$pageName.'.view.php');
        else
            throw new Exception('Le path ne doit pas demander le chargement d\'un dossier parent.', 5);
    }

    private function useController(string $pageName) : void {
        if(!preg_match('#\.\.[/\\\]#', $pageName)) {
            global $controllers;
            ($controllers->get($pageName))();
        }
        else
            throw new Exception('Le path ne doit pas demander le chargement d\'un dossier parent.', 5);
    }

    private function getPage($pageName) {
        foreach($this->listPages as $actualPage) {
            if($actualPage->getName() == $pageName)
                return $actualPage;
        }
        return false;
    }

    public function setMainPage(string $newLink) {
        $this->mainPageId = __DIR__.'/../../../view/'.$newLink;
    }

    public function setError404Page(string $newLink) {
        $this->error404Id = __DIR__.'/../../../view/'.$newLink;
    }
}