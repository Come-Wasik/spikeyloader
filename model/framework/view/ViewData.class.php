<?php
class ViewData {
    private $content = [];

    public function getContent(string $key) {
        return $this->content[$key];
    }

    public function setContent(string $key, $content) {
        $this->content[$key] = $content;
    }
}