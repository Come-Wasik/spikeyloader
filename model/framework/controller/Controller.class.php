<?php
class Controller {
    private $name;
    private $actions;

    public function __construct($name, $actions) {
        $this->setName($name);
        $this->setActions($actions);
    }

    public function getName() : string { return $this->name; }
    public function getActions() { return $this->actions; }

    public function setName(string $newData) : void { $this->name = $newData; }
    public function setActions($newData) : void { $this->actions = $newData; }
}