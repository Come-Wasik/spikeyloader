<?php
require_once(__DIR__.'/Controller.class.php');

class ControllersManager {
    private $controllers = [];

    public function add(string $name, $actions) : bool {
        if(!preg_match('#\.\.[/\\\]#', $name)) {
            $this->controllers[] = new Controller($name, $actions);
            return true;
        }
        else
            return false;
    }

    public function get(string $name) {
        foreach($this->controllers as $controller) {
            if($controller->getName() == $name)
                return $controller->getActions();
        }
        return null;
    }

    public function getList() : object {
        return $this->controllers;
    }
}