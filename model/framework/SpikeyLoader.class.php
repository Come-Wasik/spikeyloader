<?php

class SpikeyLoader {
    private $router;
    private $controllers;
    private $viewData;

    public function __construct() {
        // Inclusion des classes pour la MVC
        require_once(__DIR__.'/router/Router.class.php');
        require_once(__DIR__.'/controller/ControllersManager.class.php');
        require_once(__DIR__.'/view/ViewData.class.php');
        
        // Démarrage des éléments actifs MVC
        $this->router = new Router();
        $this->controllers = new Controllersmanager();
        $this->viewData = new ViewData();

        //Inclusion de la liste des modèles
        require_once(__DIR__.'/../../config/include_model.php');
    }

    public function getRouter() : object { return $this->router; }
    public function getControllers() : object { return $this->controllers; }
    public function getViewData() : object { return $this->viewData; }

    public function start() {
        // Lecture des controllers (pages du site web)
        require_once(__DIR__.'/../../controller/controllers.php');

        // Lecture du routeur (pages du site web)
        require_once(__DIR__.'/../../router/router.php');
        $this->router->start();
    }
}