# SpikeyLoader

SpikeyLoader is a personnal project that I have between class and company.

It's a framework who select automatiquely a controller and view in relation to the router.

To use it, remember you that router generate a page.
With this page you can put a name and an option :
    + SHOW_ONLY allow to make frontend page
    + CONTROL_ONLY allow to make backend page
    + CONTROL_AND_SHOW allow to make fullstack page

According to the following router, you will create files (controller and/or view) with the same name that you was put for a page in the router.

More :
    + controllers.php is used if you select CONTROL_ONLY or CONTROL_AND_SHOW
    + The view is used  if you select SHOW_ONLY or CONTROL_AND_SHOW
